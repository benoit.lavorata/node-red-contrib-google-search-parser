const scraper = require('google-search-scraper');
const onMessage = async (msg) => {
        return new Promise((res) => {
                var limit = msg.limit ? msg.limit : 3;
                var timeout = msg.timeout ? msg.timeout : 4500;
                var options = {
                        query: msg.payload,
                        limit: limit
                };
                var sentMsg = false;
                var results = [];
                scraper.search(options, function (err, url, meta) {
                        // This is called for each result
                        if (err) throw err;
                        meta.url = url;
                        if (results.length < limit) {
                                results.push(meta);
                        }
                        if (results.length === limit) {
                                sentMsg = true;
                                res(results);
                        }
                });

                setTimeout(() => {
                        if (!sentMsg) {
                                res(results);
                        } else {
                                throw new Error("timeout")
                        }
                }, timeout);
        });
}
module.exports = {
        onMessage: onMessage
}